@extends('layouts.base')
@include('libs.datatable')
@include('libs.actions')
@include('libs.inputmask')
@include('libs.inputmask-js')
@push('css')
    <link rel="stylesheet" href="{{ asset('css/form.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/bootstrap-select/bootstrap-select.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/summernote/summernote.min.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/assets/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/redirect/jquery.redirect.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('libs/jquery/input-mask-khp/jquery.inputmask.bundle.js') }}"></script>

    <script src="{{ asset('libs/assets/RobinHerbots/Inputmask/js/inputmask.js') }}"></script>
    <script src="{{ asset('libs/assets/RobinHerbots/Inputmask/js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('libs/assets/RobinHerbots/Inputmask/js/inputmask.numeric.extensions.js') }}"></script>
    <script src="{{ asset('libs/assets/summernote/summernote.min.js') }}"></script>
    
@endpush
@section('side-header')

@endsection
