<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data Barang</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Kode Barang</label>
            <input type="text" name="kode_barang" class="form-control" placeholder="Kode Barang" value="{{ $record->kode_barang }}">
        </div>
    	<div class="form-group field">
            <label class="control-label">Barang</label>
            <input type="text" name="nama_barang" class="form-control" placeholder="Barang" value="{{ $record->nama_barang }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>