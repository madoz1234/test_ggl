<?php
namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('sideMenu', function ($menu) {
	        $menu->add('Master', 'master')
                 ->data('icon', 'fa fa-book');
                 $menu->master->add('Barang', 'master/barang/')
                      ->active('master/barang/*');
                 $menu->master->add('Stok', 'master/stok/')
                      ->active('master/stok/*');
	        $menu->add('User', 'setting')
                 ->data('perms', 'setting')
                 ->data('icon', 'fa fa-users')
                 ->active('setting/*');
        });
        return $next($request);
    }
}
