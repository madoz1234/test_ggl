<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Setting\UserRequest;
use App\Models\Auths\User;

class UserController extends Controller
{
    protected $routes = 'setting.users';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nip',
                'name' => 'nip',
                'label' => 'NIP',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '80px',
            ],
            [
                'data' => 'name',
                'name' => 'name',
                'label' => 'Nama',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '170px',
            ],
            [
                'data' => 'tgl',
                'name' => 'tgl',
                'label' => 'Tanggal Lahir',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '170px',
            ],
            [
                'data' => 'jk',
                'name' => 'jk',
                'label' => 'Jenis Kelamin',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '170px',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'label' => 'Email',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'alamat',
                'name' => 'alamat',
                'label' => 'Alamat',
                'sortable' => true,
                'width' => '300px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'label' => 'Status',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '170px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '120px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = User::select('*');
        if($name = request()->name) {
            $records->where('name', 'like', '%' . $name . '%');
        }
        if($email = request()->email) {
            $records->where('email', 'like', '%' . $email . '%');
        }
        if($role = request()->role) {
            $records->whereHas('roles', function($u) use($role){
            	$u->where('id', $role);
            });
        }
        return DataTables::of($records)
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->editColumn('nip', function ($record) {
                   return $record->nip;
               })
               ->editColumn('tgl', function ($record) {
                   return DateToString($record->tgl_lahir);
               })
               ->editColumn('jk', function ($record) {
                    if($record->jk == 0){
	                    return 'Laki - Laki'; 
	                }else{
	                    return 'Perempuan';
	                }
               })
               ->addColumn('role', function ($record) {
                   return $record->roles->first()
                         ? $record->roles->first()->name
                         : '-';
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('alamat', function ($record) {
                   return $record->readMoreText('alamat', 150);
               })
               ->addColumn('status', function ($record) {
                    if($record->status == 1){
	                    return '<span class="label label-success">Aktif</span>'; 
	                }else{
	                    return '<span class="label label-danger">Tidak Aktif</span>';
	                }
               })
               ->addColumn('action', function ($record){
                    $buttons = '';
                    $buttons .= $this->makeButtons([
	           			'type' 		=> 'edit',
	           			'label' 	=> '<i class="fa fa-eye text-primary"></i>',
	           			'tooltip' 	=> 'Detail',
	           			'class' 	=> 'detail button',
	           			'id'   		=> $record->id,
	           		]);
                    $buttons .='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                    $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                    ]);
                    if($record->id !== auth()->user()->id) {
                       $buttons .= $this->makeButton([
                            'type' => 'delete',
                            'id'   => $record->id,
                        ]);
                  	}

                   return $buttons;
               })
               ->rawColumns(['action','status','alamat','jk'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('settings.user.index');
    }

    public function create()
    {
        return $this->render('settings.user.create');
    }

    public function store(UserRequest $request)
    {
    	if($request['status']){
    		$status = 1;
    	}else{
    		$status = 0;
    	}
		$user = User::create([
			'nip' 		=> $request['nip'],
			'name' 		=> $request['name'],
            'email' 	=> $request['email'],
            'status' 	=> $status,
            'tgl_lahir' => $request['tgl_lahir'],
            'jk' 		=> $request['jk'],
            'alamat' 	=> $request['alamat'],
            'password' 	=> Hash::make($request['password']),
        ]);
	    $user->roles()->sync($request->role);

        return $user;
    }

    public function detail(User $id)
    {
       return $this->render('settings.user.detail', ['record' => $id]);
    }

    public function edit(User $user)
    {
        return $this->render('settings.user.edit', ['record' => $user]);
    }

    public function update(UserRequest $request, User $user)
    {
    	if($request['status']){
    		$status = 1;
    	}else{
    		$status = 0;
    	}
		$attrs = [
			'nip' 		=> $request['nip'],
			'name' 		=> $request['name'],
            'email' 	=> $request['email'],
            'status' 	=> $status,
            'tgl_lahir' => $request['tgl_lahir'],
            'jk' 		=> $request['jk'],
            'alamat' 	=> $request['alamat'],
        ];
        if ($pass = $request->password) {
            $attrs['password'] = Hash::make($pass);
        }
        $user->update($attrs);
	    $user->roles()->sync($request->role);
        return $user;
    }

    public function destroy(User $user)
    {
        if($user){
    		$user->delete();
        	return response([
            	'status' => true,
	        ],200);
    	}else{
    		return response([
                'status' => 500,
            ],500);
    	}
    }
}
