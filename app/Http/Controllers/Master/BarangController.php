<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Task\TaskRequest;
use App\Models\Auths\User;
use App\Models\Master\Barang;

use DB;

class BarangController extends Controller
{
    protected $routes = 'master.barang';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'kode_barang',
                'name' => 'kode_barang',
                'label' => 'Kode Barang',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '80px',
            ],
            [
                'data' => 'nama_barang',
                'name' => 'nama_barang',
                'label' => 'Nama Barang',
                'sortable' => true,
                'className' => 'text-center',
                'width' => '170px',
            ],
            [
                'data' => 'img',
                'name' => 'img',
                'label' => 'Image',
                'sortable' => true,
                'width' => '170px',
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '100px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '70px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Barang::select('*');
        if($kode_barang = request()->kode_barang) {
            $records->where('kode_barang', 'like', '%' . $kode_barang . '%');
        }
        return DataTables::of($records)
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->editColumn('kode_barang', function ($record) {
                   return $record->kode_barang;
               })
               ->editColumn('nama_barang', function ($record) {
                   return $record->nama_barang;
               })
               ->editColumn('img', function ($record) {
                   return '';
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record){
                    $buttons = '';
	                $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                    ]);
                    
                    $buttons .= $this->makeButton([
                        'type' => 'delete',
                        'id'   => $record->id,
                    ]);

                   return $buttons;
               })
               ->rawColumns(['action','status','alamat','jk'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.barang.index');
    }

    public function create()
    {
        return $this->render('modules.barang.create');
    }

    public function store(Request $request)
    {
    	DB::beginTransaction();
    	try {
	    	$simpan 				= new Barang;
		    $simpan->kode_barang 	= $request->kode_barang;
		  	$simpan->nama_barang 	= $request->nama_barang;
		    $simpan->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function edit(Barang $barang)
    {
        return $this->render('modules.barang.edit', ['record' => $barang]);
    }

    public function update(Request $request, Barang $barang)
    {
    	DB::beginTransaction();
    	try {
	    	$barang 				= Barang::find($request->id);
		    $barang->kode_barang 	= $request->kode_barang;
		  	$barang->nama_barang 	= $request->nama_barang;
		    $barang->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Barang $barang)
    {
        if($barang){
    		$barang->delete();
        	return response([
            	'status' => true,
	        ],200);
    	}else{
    		return response([
                'status' => 500,
            ],500);
    	}
    }
}
