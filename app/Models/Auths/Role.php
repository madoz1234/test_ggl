<?php 
namespace App\Models\Auths;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Utilities;


class Role extends Model
{
    use Utilities;

    public $table = 'sys_roles';

    protected $fillable = ['name',];
	
}