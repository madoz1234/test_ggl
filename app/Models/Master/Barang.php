<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Auths\User;
use App\Models\Master\Stok;

class Barang extends Model
{
    /* default */
    protected $table 		= 'ref_barang';
    protected $fillable 	= ['kode_barang','nama_barang'];

    /* data ke log */
    /* relation */
    public function stok(){
        return $this->hasOne(Stok::class, 'id_barang');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
