<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Auths\User;
use App\Models\Master\Barang;

class Stok extends Model
{
    /* default */
    protected $table 		= 'ref_stok';
    protected $fillable 	= ['id_barang','total_barang','total_barang','jenis_stok'];

    /* data ke log */
    /* relation */
    public function barang(){
        return $this->belongsTo(Barang::class, 'id_barang');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
