<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', function(){
  return redirect()->route('dashboard.home.index');
});

Route::middleware('auth')->group(function() {
	Route::group(['prefix' => 'ajax/option', 'namespace' => 'Ajax'], function(){
	    Route::post('get-peta', 'ChainOptionController@getPeta');
	});

	Route::name('master.')->prefix('master')->namespace('Master')->group( function() {
        Route::post('barang/grid', 'BarangController@grid')->name('barang.grid');
		Route::resource('barang', 'BarangController');

		Route::post('stok/grid', 'StokController@grid')->name('stok.grid');
		Route::resource('stok', 'StokController');
	});

	Route::name('setting.')->prefix('setting')->namespace('Setting')->group( function() {
		Route::get('/', function(){
			return redirect()->route('setting.users.index');
		});

		Route::post('users/grid', 'UserController@grid')->name('users.grid');
		Route::get('users/{id}/detail','UserController@detail')->name('users.detail');
		Route::resource('users', 'UserController');

		Route::post('log/grid', 'LogController@grid')->name('log.grid');
		Route::resource('log', 'LogController');

		Route::post('roles/grid', 'RoleController@grid')->name('roles.grid');
		Route::patch('roles/saveData/{id}','RoleController@saveData')->name('roles.saveData');
		Route::resource('roles', 'RoleController');
	});
});

Auth::routes();
