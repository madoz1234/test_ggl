<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_barang', 255);
            $table->string('nama_barang', 255);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_stok', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_barang')->unsigned()->default(1);
            $table->integer('total_barang');
            $table->integer('jenis_stok')->default(0)->comment('0:In,1:Out');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('id_barang')->references('id')->on('ref_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_stok');
        Schema::dropIfExists('ref_barang');
    }
}
